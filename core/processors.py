from django.conf import settings

def global_settings(request):
    return {
        'TITLE': settings.BRAND_NAME
    }